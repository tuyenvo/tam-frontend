$(document).ready(function () {
    $('#news_filter').selectpicker({
        style: 'select',
        size: 4
    });
    $('.video-modal .play-video').on('click', function(ev) {

        $('.video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
        ev.preventDefault();
    });
    $('#videoModal').on('hidden.bs.modal', function (ev) {
        $('.video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        ev.preventDefault();
    });

    $('.six-slide').owlCarousel({
        navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
        items: 6,
        slideBy: 6,
        thumbs: false,
        dots: false,
        loop:  false,
        nav: true
    });

    $('.thumb-slide').owlCarousel({
        items: 1,
        navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
        dots: false,
        loop:  true,
        nav: true,
        thumbs: true,
        thumbImage: true,
        thumbsPrerendered: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item'
    });
    $('.nav-tabs a').on('shown.bs.tab', function() {
        $(this).parent().addClass('active');
        var others = $(this).parent().parent().siblings();
        others.each(function () {
            var item = $(this).children();
            if(item.hasClass('active')) {
                $(item).removeClass('active');
            }
        });
    });
});