

(function ($) {
    $(document).ready(function () {
        // Thumbnail
        if ($(".middle_img").length > 0) {
            $('.middle_img').each(
                function(){
                    var height = $(this).find('img').outerHeight(),
                        width = $(this).find('img').outerWidth();
                    if(height > width){
                        $(this).find('img').css({'width': '100%'});
                    }else{
                        $(this).find('img').css({'height': '100%'});
                    }
                }
            )
        }

        /* Menu mobile */
        $('.menu-btn').click(function(){
            if($('body').hasClass('showMenu')){
                $('body').removeClass('showMenu');
            }else{
                $('body').addClass('showMenu');
                //$('.flexMenuToggle:first').click();
            return false;
            }
        }); 
    

    // Click scroll a
    $(".scroll").on('click', function (event) {
      event.preventDefault();
      $('html,body').animate({scrollTop: 0 }, 1000);
    });




        //OWL----------------------------------------------------

        $('.single-slide').each(function () {
           
            $(this).owlCarousel({
                navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
                items:1,
                autoplayHoverPause:false,
                autoplay: true,
                thumbs: false,
                dots: $(this).hasClass('s-dots') ? true : false,
                loop: true,
                lazyLoad: $(this).hasClass('s-lazy') ? true : false,
                nav: $(this).hasClass('s-nav') ? true : false,
               
            })
        });

        $('.home-partner .owl-carousel').each(function () {
            $(this).owlCarousel({
                navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
                items:7,
                autoplayHoverPause:false,
                autoplay: true,
                thumbs: false,
                dots: $(this).hasClass('s-dots') ? true : false,
                loop: true,
                lazyLoad: $(this).hasClass('s-lazy') ? true : false,
                nav: $(this).hasClass('s-nav') ? true : false,
                responsive:{
                    0:{
                        items:2
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:7
                    }
                }
            })
        });

        $('.home-service .owl-carousel').each(function () {
            $(this).owlCarousel({
                navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
                autoplayHoverPause:false,
                autoplay: true,
                thumbs: false,
                dots: $(this).hasClass('s-dots') ? true : false,
                loop: true,
                lazyLoad: $(this).hasClass('s-lazy') ? true : false,
                nav: $(this).hasClass('s-nav') ? true : false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:4
                    }
                }
            })
        });

        $('.slide-one').each(function () {
            $(this).owlCarousel({
                navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
                items:1,
                thumbs: false,
                autoplayHoverPause:false,
                autoplay: true,
                dots: $(this).hasClass('s-dots') ? true : false,
                loop: true,
                lazyLoad: $(this).hasClass('s-lazy') ? true : false,
                nav: $(this).hasClass('s-nav') ? true : false,
                
            })
        });

        $('.iframe .play-video').on('click', function(ev) {

            $(this).hide();
            $(this).siblings('.video')[0].src += "&autoplay=1";
            ev.preventDefault();
         
          });


        // iframe video
        var getVideoUrl = function(id){
            return 'https://www.youtube.com/embed/' + id + '?rel=0&autoplay=1';
        }
        var addVideoUrl = function(id){
            return 'https://www.youtube.com/embed/' + id + '?rel=0';
        }

        var modal = $('#myVideo').on('hidden.bs.modal',function(){
            modal.find('iframe').removeAttr('src');
        });
        var fullVideo = $('#fullVideo');

        
        $(document).on('click','.block-video',function(){
            var btnVideo = $(this);    
            var video = btnVideo.attr('data-video');     
            //modal.find('iframe').attr('src', getVideoUrl(video));
            fullVideo.addClass('show-video');
            fullVideo.find('iframe').attr('src', getVideoUrl(video));
        });  

        // single video
        $(document).on('click','.single-video .item_video',function(){
            var btnVideo = $(this);    
            var video = btnVideo.attr('data-video');
            var parent = btnVideo.parent().parent();     
            if(parent.hasClass('show-video')){
                    parent.removeClass('show-video');
                    parent.find('iframe').removeAttr('src');
            }else{
                    parent.addClass('show-video');
                    parent.find('iframe').attr('src', getVideoUrl(video));
            };  
        });  




        // slideToggle
        $('.block-toggle .block-title').each(function(){
            var btnTg = $(this).click(function(){ 
                if(btnTg.parent().hasClass('active')){
                    btnTg.parent().removeClass('active');
                    btnTg.parent().find('.block-content').slideUp(300);                                           
                }else{
                    $('.block-toggle .block-item').removeClass('active');
                    $('.block-toggle .block-content').slideUp(300);
                    btnTg.parent().addClass('active');
                    btnTg.parent().find('.block-content').slideDown(300);               
                };  
            });
        });       




        // Menu
        $('ul.menu_main li.parent, #footer ul.menu li.parent ').each(function(){
             var 
                p = $(this),
                btn = $('<span>',{'class':'showsubmenu fa fa-caret-down', text : '' }).click(function(){
                    if(p.hasClass('parent-showsub')){
                        menu.slideUp(300,function(){
                            p.removeClass('parent-showsub');
                        });                                             
                    }else{
                        menu.slideDown(300,function(){
                            p.addClass('parent-showsub');
                        });                                             
                    };  
                }),
                menu = p.children('ul')
             ;
             p.prepend(btn) 
        });

        // section background
        $('.section-background').each(function(){
            var filename = $(this).find('.img-background').attr('src').replace( /^.*?([^\/]+)\.jpg?$/, '$1' );
            $(this).css('background-image', 'url(asset/images/' + filename + '.jpg)');
        })  


        /* Equal Height good*/
        function equalHeight(className, padding) {
          var tempHeight = 0;
          $(className).each(function () {
            current = $(this).height();
            if (parseInt(tempHeight) < parseInt(current)) {
              tempHeight = current;
            }
          });
          $(className).css("minHeight", tempHeight + padding + "px");
        }

        
        $(window).bind("load", function() {
            // Lazy img 
            $('.img-lazy').each(function(){
                var src = $(this).attr('data-src');
                $(this).attr('src', src);
                $(this).removeClass('img-lazy').addClass('img-loaded');
            }); // end Lazy img 
            // Lazy bg 
            $('.bg-lazy').each(function(){
                var src = $(this).attr('data-src');
                $(this).css('background-image', 'url(' + src + ')');
                $(this).removeClass('bg-lazy').addClass('bg-loaded');
            }); // end Lazy bg 

        });    

        // Facebook
            $('.fb-messenger .btnFb').click(function(){

                $('.fb-messenger').toggleClass('active');

                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));


            }); 
                

     /* slide TAM003 */
        $(document).ready(function () {
            var sync1 = $(".TAM003 #sync1");
              var sync2 = $(".TAM003 #sync2");
              var slidesPerPage = 5; //globaly define number of elements per page
              var syncedSecondary = true;

              sync1.owlCarousel({
                items : 1,
                slideSpeed : 2000,
                nav: false,
                autoplay: true,
                dots: false,
                loop: true,
                autoHeight: true,
                responsiveRefreshRate : 200,
                navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
              }).on('changed.owl.carousel', syncPosition);

              sync2
                .on('initialized.owl.carousel', function () {
                  sync2.find(".owl-item").eq(0).addClass("current");
                })
                .owlCarousel({
                items : slidesPerPage,
                dots: false,
                margin: -50,
                navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
                nav:true,
                singleItem: true,
                smartSpeed: 200,
                slideSpeed : 500,
                slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                responsiveRefreshRate : 100,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:3
                    }
                }
              }).on('changed.owl.carousel', syncPosition2);

              function syncPosition(el) {
                //if you set loop to false, you have to restore this next line
                //var current = el.item.index;
                
                //if you disable loop you have to comment this block
                var count = el.item.count-1;
                var current = Math.round(el.item.index - (el.item.count/2) - .5);
                
                if(current < 0) {
                  current = count;
                }
                if(current > count) {
                  current = 0;
                }
                
                //end block

                sync2
                  .find(".owl-item")
                  .removeClass("current")
                  .eq(current)
                  .addClass("current");
                var onscreen = sync2.find('.owl-item.active').length - 1;
                var start = sync2.find('.owl-item.active').first().index();
                var end = sync2.find('.owl-item.active').last().index();
                
                if (current > end) {
                  sync2.data('owl.carousel').to(current, 100, true);
                }
                if (current < start) {
                  sync2.data('owl.carousel').to(current - onscreen, 100, true);
                }
              }
              
              function syncPosition2(el) {
                if(syncedSecondary) {
                  var number = el.item.index;
                  sync1.data('owl.carousel').to(number, 100, true);
                }
              }
              
              sync2.on("click", ".owl-item", function(e){
                e.preventDefault();
                var number = $(this).index();
                sync1.data('owl.carousel').to(number, 300, true);
              });


        });

        /* add-icon */
        var windowsize = $(window).width();
        if (windowsize < 768) {
            var item = $('.TAM003 #sync2 .item');
            if (item.length > 3) { 
                $('.TAM003 .owl-carousel').addClass('add-icon');
            }
            else{
                $('.TAM003 .owl-carousel').removeClass('add-icon');
            }
        }   
            

        /* scroll menu */
        var nav = $('#header');
        var pos = nav.offset().top;
        $(window).scroll(function () {
            var fix = ($(this).scrollTop() > pos) ? true : false;
            nav.toggleClass("fix-nav", fix);
            $('body').toggleClass("fix-body", fix);
        
        });

            if($('.welcome').length) {
                parallax();
            }
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });



    });
})(jQuery); 