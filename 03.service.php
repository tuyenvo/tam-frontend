<?php include 'include/index-top.php';?>

	<div id="contentmain" class="service">
        <?php include 'module/module-1.php';?>
        <div class="breadcrumb">
            <div class="container">
                <ul class="block-breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li><a href="#">Tin tức</a></li>
                </ul>
            </div>
        </div>
        <section id="service_list">
            <div class="container">
                <div class="six-slide owl-carousel s-nav">
                    <div class="item active"><a href="#">Nâng cung chân mày</a></div>
                    <div class="item"><a href="#">Nâng cơ mặt</a></div>
                    <div class="item"><a href="#">Nâng cơ đùi</a></div>
                    <div class="item"><a href="#">Nâng cơ bụng</a></div>
                    <div class="item"><a href="#">Nâng cơ mông</a></div>
                    <div class="item"><a href="#">Nâng cơ da cổ</a></div>
                    <div class="item"><a href="#">Nâng cơ 7</a></div>
                    <div class="item"><a href="#">Nâng cơ 8</a></div>
                    <div class="item"><a href="#">Nâng cơ 9</a></div>
                </div>

                <div class="service_list_content">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="owl-thumbs" data-slider-id="thumb"></div>
                        </div>
                        <div class="col-md-7">
                            <div class="thumb-slide owl-carousel" data-slider-id="thumb">
                                <img src="asset/images/service-slide.jpg" alt="Service Slide">
                                <img src="asset/images/service-slide.jpg" alt="Service Slide">
                                <img src="asset/images/service-slide.jpg" alt="Service Slide">
                                <img src="asset/images/service-slide.jpg" alt="Service Slide">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h2 class="titlemain mt-50 mb-30">Nâng cơ da là gì?</h2>
                            <p>
                                Nâng cơ da là 1 phương pháp tái tạo da mặt và nâng cơ chảy xệ về 1 vị trí cố định làm giảm các dấu hiệu tuổi tác bằng trọng lực cho da săn chắc,giúp bạn trở nên trẻ trung hơn.Khi tuổi càng cao,lớp collagen và elastine dưới da càng mất dần đi khiến da kém đàn hồi và dần chảy xệ xuống. Bên cạnh đó, những tác động từ môi trường như tia UVA, UVB có ảnh hưởng nghiêm trọng đến các tế bào da, khiến các sợi liên kết dưới da bị đứt gãy, mất đi tính đàn hồi và thúc đẩy quá trình lão hóa diễn ra nhanh hơn. Chính vì thế, giải pháp nâng cơ được nhắc đến thường xuyên như một “vị cứu tinh” cho làn da dần trở lại tuổi thanh xuân
                            </p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
        <section id="service_technology">
            <div class="tech_img-left">
                <img src="asset/images/service-left.jpg" alt="">
            </div>
            <div class="tech_img-left"></div>
        </section>
	</div>




<?php include 'include/index-bottom.php';?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript">window.TAM = window.TAM || {};
    TAM.GooglePhotos = (function($) {
        var mapData = function(data, map) {
            $.each(map, function(key, val) {
                if (data[key] != undefined) {
                    data[val] = data[key];
                    delete data[key];
                }
            });
            return data;
        }
        return $.extend(function(options, data) {
            var self = arguments.callee;
            options = $.extend({}, self.options, options);
            var app = angular.module(options.module, []);
            var getEndOfRow = function(element) {
                var endEl = $(element),
                    currentTop = endEl.position().top,
                    nextEl;
                while (true) {
                    nextEl = endEl.next();
                    while (nextEl.length && !nextEl.hasClass('photo')) nextEl = nextEl.next();
                    if (!nextEl.length) break;
                    if (nextEl.position().top > currentTop) break;
                    endEl = endEl.next();
                }
                return endEl;
            }
            app.controller('GooglePhotos', ['$scope', function(scope) {
                var viewPort, viewportPos, transitionEvts = 'transitionend webkitTransitionEnd oTransitionEnd';
                $.extend(scope, {
                    model: {
                        projects: (data || []).map(function(project) {
                            return mapData(project, options.map);
                        })
                    },
                    viewport: {},
                    showDetail: function(index) {
                        index = index < 0?0:index;
                        index = index > scope.model.projects.length - 1? scope.model.projects.length - 1: index;
                        scope.model.projectIndex = index;
                        var panel = scope.viewport.el(),project = scope.model.projects[index];
                        project.element.parent().children('.active').removeClass('active');
                        if (project === scope.viewport.model.project) {
                            if (panel.is(':visible')) {
                                this.closeDetail();
                            } else {
                                panel.addClass('animating').slideDown(options.duration, function() {
                                    panel.removeClass('animating');
                                });
                                project.element.addClass('active');
                            }
                        } else {
                            scope.refreshViewport(project, true);
                            scope.viewport.show(project);
                            project.element.addClass('active');
                        }
                    },
                    closeDetail: function() {
                        this.viewport.el().addClass('animating').slideUp(options.duration, function() {
                            $(this).removeClass('animating')
                        });
                    },
                    prevDetail: function(){ scope.showDetail(scope.model.projectIndex - 1)},
                    nextDetail: function(){ scope.showDetail(scope.model.projectIndex + 1)},
                    dateFormat: function(date) {
                        return date;
                    },
                    refreshViewport: function(project, animate) {
                        if (!project) return;
                        var
                            isVisible = scope.viewport.el().is(':visible'),
                            viewport = scope.viewport.el().hide(),
                            endEl = getEndOfRow(project.element);
                        viewport.show();
                        if (!endEl.is(viewportPos)) {
                            if (animate) {
                                if (isVisible) {
                                    var holder = viewport.clone().insertAfter(viewport).addClass('animating').slideUp(options.duration, function() {
                                        holder.remove();
                                    });
                                }
                                viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                    viewport.removeClass('animating');
                                });
                            }
                            endEl.after(viewport);
                            viewportPos = endEl;
                        } else if (animate && !isVisible) {
                            viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                viewport.removeClass('animating');
                            });
                        }
                    }
                });
                $(window).resize(function() {
                    scope.refreshViewport(scope.viewport.model.project);
                });
            }]).controller('GooglePhotoDetail', ['$scope', '$element', function(scope, element) {
                scope.$parent.viewport = scope;


                var
                    $scoll = $('html,body'),
                    owl = element.find('.images');
                $.extend(scope, {
                    model: {},
                    el: function() {
                        return element;
                    },
                    project: {},
                    close: function() {},
                    show: function(project) {
                        if (this.model.project === project) return;
                        this.model.project = project;
                        owl.find('.owl-stage').children().each(function(index){
                            owl.trigger('remove.owl.carousel', [index]);
                        });

                        $.each(project.images,function(i,img){
                            owl.trigger('add.owl.carousel', [$('<div class="item">').append($('<img>',{src:img})), i]);
                        });
                        owl.trigger('refresh.owl.carousel');
                        setTimeout(function(){
                            $scoll.stop().animate({scrollTop: element.offset().top - 100});
                        },300);
                    }
                });
            }]).directive('googlePhotoElement', function() {
                var propName;
                return function(scope, element, attrs) {
                    propName = propName || $.trim(attrs.ngRepeat.split(' in ')[0]);
                    scope[propName].element = element;
                };
            });
        }, {
            options: {
                module: 'TAM',
                map: function(project) {
                    return project;
                },
                duration: 300
            }
        })
    })(jQuery);
    var exampleData = (function(count) {
        var arr = [];
        for (var i = 0; i < count; i++) {
            arr.push({
                id: i + 1,
                title: 'Tên dịch vụ',
                date: '16 Feb 2017',
                thumbBefore: 'asset/images/Rectangle10.jpg',
                thumbAfter: 'asset/images/after.jpg',
                shortDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                fullDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                images: ['http://via.placeholder.com/1000x1000', 'http://via.placeholder.com/1500x1000', 'http://via.placeholder.com/1200x1000', 'http://via.placeholder.com/1500x1200'],
                colors: [{
                    title: 'Nau',
                    hex: 'ff0000',
                    id: 12453
                }]
            });
        }
        return arr;
    })(12);
    TAM.GooglePhotos({
        module: 'TAM'
    }, exampleData);
</script>