<?php include 'include/index-top.php';?>
	<div id="contentmain">
		<div class="container">	
			<ul class="block-breadcrumb">
				<li><a href="#">Trang chủ</a></li>
				<li><a href="#">Liên hệ</a></li>
			</ul>
			<h2 class="titlemain">Liên hệ</h2>
		</div>	
		<section class="form-contact bg-left">
			<div class="container">
				<div class="row">	
					<div class="col-md-4 ">
						<div class="block-contact">
							<div class="section-header">
								<h2 class="section-title">
									Tâm Spa & Clinic
								</h2>
								<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
							</div>
							<ul class="">	
								<li>0977 954 800</li>
								<li>LoremIpsum@gmail.com</li>
								<li>www.Lorem Ipsum</li>
								<li>www.Lorem Ipsum</li>
							</ul>
						</div>
						<div class="block-social">
							<ul>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							</ul>
						</div><!-- end block-social-->
						<div class="hotline">1800 2058</div>
					</div>
					<div class="col-md-8 tab-form">
						
						<ul  class="nav">
							<li class="active">
					        	<a  href="#1a" data-toggle="tab">Thông tin nhận quà trải nghiệm</a>
							</li>
							<li>
								<a href="#2a" data-toggle="tab">Đặt lịch tư vấn</a>
							</li>
						</ul>
						<div class="tab-content clearfix">
							<div class="tab-pane active" id="1a">
					          	<form action="" class="row">
					          		<div class="col-md-6">
						          		<input type="text" placeholder="Họ tên" class="input bg-input">
						          		<input type="text" placeholder="Điện thoại" class="input bg-input">
						          		<select name="" id="" class="select bg-input">
											<option>Giới tính</option>
											<option value="Nam">Nam</option>
											<option value="Nữ">Nam</option>
										</select>
					          		</div>
					          		<div class="col-md-6">
						          		<input type="text" id="datepicker" placeholder="Ngày sinh" class="input bg-input">
						          		<input type="text" placeholder="Email" class="input bg-input">
						          		<input type="text" placeholder="Điạ chỉ" class="input bg-input">
					          		</div>
					          		<div class="col-md-12">
					          			<textarea name="" id="" class="textarea bg-input" rows="2" placeholder="Nội dung"></textarea>
					          			<button class="btn btn-big">GỬI</button>
					          		</div>
					          	</form>
							</div>
							<div class="tab-pane" id="2a">
					          	<form action="" class="row">
					          		<div class="col-md-6">
						          		<input type="text" placeholder="Họ tên" class="input bg-input">
						          		<input type="text" placeholder="Điện thoại" class="input bg-input">
						          		<select name="" id="" class="select bg-input">
											<option>Giới tính</option>
											<option value="Nam">Nam</option>
											<option value="Nữ">Nam</option>
										</select>
					          		</div>
					          		<div class="col-md-6">
						          		<input type="text" id="datepicker" placeholder="Ngày sinh" class="input bg-input">
						          		<input type="text" placeholder="Email" class="input bg-input">
						          		<input type="text" placeholder="Điạ chỉ" class="input bg-input">
					          		</div>
					          		<div class="col-md-12">
					          			<textarea name="" id="" class="textarea bg-input" rows="2" placeholder="Nội dung"></textarea>
					          			<button class="btn btn-big">GỬI</button>
					          		</div>
					          	</form>
							</div>
					        
						</div>
						
					</div>
				</div>
			</div>
		</section><!-- end form-contact -->
		<div class="title-big">
			Hệ thống chi nhánh Tâm Spa & Clinic
		</div>

		<section id="ss-map0">
		    <div class="container">
				<div class="row ">
					<div class="col-md-7 col-md-offset-3 col-sm-offset-2">
						
						<div class="item">
			                <select name="city" class="selectpicker" id="slb_city">
			                    <option value="10.7687085-106.4141728">Tp Hồ Chí Minh</option>
			                    <option value="21.0227431-05.8194541">Hà Nội</option>
			                </select>				
						</div>
						<div class="item">
			                <select name="district" class="selectpicker" id="slb_district">   </select>
						</div>
						<div class="item">
							<button class="btn btn-big"><i class="fa fa-search" aria-hidden="true"></i></button>
						</div>
					
					</div>
				</div>
	    	</div>    
		</section>
			
		<section id="ss-map" class="bg-1">
			<div class="container">
				<div class="map_list ">
					<ul class="map-list-store">
					</ul>
				</div>
			</div>
			<div class="wrap-map"><div id="map" style="height: 450px"></div></div>    
		</section>
		<section class="TAM002 bg-gray">
			<div class="container">
				<div class="slide-one owl-carousel s-dots s-nav">	
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
				</div>
			</div>
		</section><!-- end TAM002 -->	
	</div>




<?php include 'include/index-bottom.php';?>
<link rel='stylesheet' href='asset/css/bootstrap-select.css' type='text/css' media='all' />
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvkmSfZ2tgoATkurtr44Ez1nI2beFYw48"
        type="text/javascript"></script>
<script type="text/javascript" src="asset/js/bootstrap-select.min.js"></script>     
<script type="text/javascript" src="asset/js/common.js"></script>