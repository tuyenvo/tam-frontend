<?php include 'include/index-top.php';?>
	<div id="contentmain">
		<div class="container">	
			<ul class="block-breadcrumb">
				<li><a href="#">Trang chủ</a></li>
				<li><a href="#">Thành viên</a></li>
			</ul>
			<h2 class="titlemain">Thành viên</h2>
		</div>	
		

		<div id="tab-member" class="container">	
			<ul  class="nav">
				<li class="active">
	        		<a  href="#1a" data-toggle="tab">
	        			<span>Thành viên</span>
	        			<h3 class="title">Chưa sử dụng dịch vụ</h3>
	        		</a>
				</li>
				<li>
					<a href="#2a" data-toggle="tab">
						<span>Thành viên</span>
	        			<h3 class="title">Đã sử dụng dịch vụ</h3>
					</a>
				</li>
				<li>
					<a href="#3a" data-toggle="tab">
						<span>Thành viên</span>
	        			<h3 class="title">VIP</h3>
					</a>
				</li>
	  			<li>
	  				<a href="#4a" data-toggle="tab">
	  					<span>Thành viên</span>
	        			<h3 class="title">VVIP</h3>
	  				</a>
				</li>
			</ul>

			<div class="tab-content clearfix ">
			  	<div class="tab-pane active" id="1a">
	          		<div class="slide-one owl-carousel s-dots">
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>

	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          		</div>
				</div>
				<div class="tab-pane" id="2a">
	          		<div class="slide-one owl-carousel s-dots">
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>

	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          		</div>
				</div>
	        	<div class="tab-pane" id="3a">
	          		<div class="slide-one owl-carousel s-dots">
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>

	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          		</div>
				</div>
	          	<div class="tab-pane" id="4a">
	          		<div class="slide-one owl-carousel s-dots">
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>

	          			<div class="text">
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	          			</div>
	          		</div>
				</div>
			</div>
	  	</div>
		
		<section class="form-member">
			<div class="section-header">
				<h2 class="section-title">
					Đăng ký thành viên
				</h2>
			</div>
			<div class="container">
				<form action="" class="row">
	          		<div class="col-md-4">
		          		<input type="text" placeholder="Họ tên" class="input bg-input">
		          		<input type="text" id="datepicker" placeholder="Ngày sinh" class="input bg-input">
	          		</div>
	          		<div class="col-md-4">
	          			<input type="text" placeholder="Điện thoại" class="input bg-input">
	          			<select name="" id="" class="select bg-input">
							<option>Giới tính</option>
							<option value="Nam">Nam</option>
							<option value="Nữ">Nam</option>
						</select>
	          		</div>
	          		<div class="col-md-4">
		          		<input type="text" placeholder="Email" class="input bg-input">
		          		<input type="text" placeholder="Điạ chỉ" class="input bg-input">
	          		</div>
	          		<div class="col-md-12 text-center">
	          			<button class="btn btn-big">ĐĂNG KÝ</button>
	          		</div>
	          	</form>
			</div>
		</section><!-- end form-member -->

		<section class="TAM001">
			<div class="container">
				<div class="section-header">
					<h2 class="section-title">
						Các chương trình khuyến mãi
					</h2>
					<span>(Dành cho thành viên)</span>
				</div>
				<div class="slide-one owl-carousel s-dots">
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>

				</div>
			</div>
		</section><!-- end TAM001 -->
		
		<section class="TAM002 bg-gray">
			<div class="container">
				<div class="slide-one owl-carousel s-dots s-nav">	
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
				</div>
			</div>
		</section><!-- end TAM002 -->
		
		
	</div>




<?php include 'include/index-bottom.php';?>
<link rel='stylesheet' href='asset/css/bootstrap-select.css' type='text/css' media='all' />
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvkmSfZ2tgoATkurtr44Ez1nI2beFYw48"
        type="text/javascript"></script>
<script type="text/javascript" src="asset/js/bootstrap-select.min.js"></script>     
<script type="text/javascript" src="asset/js/common.js"></script>