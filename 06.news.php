<?php include 'include/index-top.php';?>
	<div id="contentmain" class="news">
        <div class="breadcrumb">
            <div class="container">
                <ul class="block-breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li><a href="#">Tin tức</a></li>
                </ul>
            </div>
        </div>

        <section id="news_blog" class="blog">
            <div class="space-breadcrumb"></div>
            <div class="container">
                <h2 class="titlemain">Blog</h2>
                <div class="blog_content">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="blog_main">
                                <img src="asset/images/news-1.jpg" alt="news-1">
                                <div class="blog_main_text">
                                    <p class="overflow-2 mb-0">Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="blog_sidebar">
                                <div class="sidebar_block clearfix">
                                    <img class="sidebar_block_img" src="asset/images/blog-1.jpg" alt="blog">
                                    <div class="sidebar_block_content">
                                        <div class="sidebar_block_meta">
                                            <i class="material-icons">event</i> <span class="sidebar_block_date">25 Oct 2017</span>
                                            <i class="material-icons">remove_red_eye</i> <span class="sidebar_block_view">12</span>
                                        </div>
                                        <p class="sidebar_block_title">Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus</p>
                                    </div>
                                </div>
                                <div class="sidebar_block clearfix">
                                    <img class="sidebar_block_img" src="asset/images/blog-1.jpg" alt="blog">
                                    <div class="sidebar_block_content">
                                        <div class="sidebar_block_meta">
                                            <i class="material-icons">event</i> <span class="sidebar_block_date">25 Oct 2017</span>
                                            <i class="material-icons">remove_red_eye</i> <span class="sidebar_block_view">12</span>
                                        </div>
                                        <p class="sidebar_block_title">Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus</p>
                                    </div>
                                </div>
                                <div class="sidebar_block clearfix">
                                    <img class="sidebar_block_img" src="asset/images/blog-1.jpg" alt="blog">
                                    <div class="sidebar_block_content">
                                        <div class="sidebar_block_meta">
                                            <i class="material-icons">event</i> <span class="sidebar_block_date">25 Oct 2017</span>
                                            <i class="material-icons">remove_red_eye</i> <span class="sidebar_block_view">12</span>
                                        </div>
                                        <p class="sidebar_block_title">Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		<section id="news_list" class="home-blog news_list">
			<div class="container">
                <div class="list_select">
                    <span class="list_select_title">Lọc theo:</span>
                    <select name="filter" id="news_filter">
                        <option value="nangco">Dịch vụ nâng cơ</option>
                        <option value="giambeo">Dịch vụ giảm béo</option>
                        <option value="trimun">Dịch vụ trị mụn</option>
                        <option value="nangco">Dịch vụ nâng cơ</option>
                        <option value="giambeo">Dịch vụ giảm béo</option>
                        <option value="trimun">Dịch vụ trị mụn</option>
                    </select>
                </div>
				<div class="row">
					<div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-2.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
					</div>
					<!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-3.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-4.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-2.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-3.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-4.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
				</div>
			</div>
		</section><!-- end home-blog -->

        <div class="container">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="active">
                        <span><span>1</span></span>
                    </li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </nav>
        </div>
	</div>




<?php include 'include/index-bottom.php';?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript">window.TAM = window.TAM || {};
    TAM.GooglePhotos = (function($) {
        var mapData = function(data, map) {
            $.each(map, function(key, val) {
                if (data[key] != undefined) {
                    data[val] = data[key];
                    delete data[key];
                }
            });
            return data;
        }
        return $.extend(function(options, data) {
            var self = arguments.callee;
            options = $.extend({}, self.options, options);
            var app = angular.module(options.module, []);
            var getEndOfRow = function(element) {
                var endEl = $(element),
                    currentTop = endEl.position().top,
                    nextEl;
                while (true) {
                    nextEl = endEl.next();
                    while (nextEl.length && !nextEl.hasClass('photo')) nextEl = nextEl.next();
                    if (!nextEl.length) break;
                    if (nextEl.position().top > currentTop) break;
                    endEl = endEl.next();
                }
                return endEl;
            }
            app.controller('GooglePhotos', ['$scope', function(scope) {
                var viewPort, viewportPos, transitionEvts = 'transitionend webkitTransitionEnd oTransitionEnd';
                $.extend(scope, {
                    model: {
                        projects: (data || []).map(function(project) {
                            return mapData(project, options.map);
                        })
                    },
                    viewport: {},
                    showDetail: function(index) {
                        index = index < 0?0:index;
                        index = index > scope.model.projects.length - 1? scope.model.projects.length - 1: index;
                        scope.model.projectIndex = index;
                        var panel = scope.viewport.el(),project = scope.model.projects[index];
                        project.element.parent().children('.active').removeClass('active');
                        if (project === scope.viewport.model.project) {
                            if (panel.is(':visible')) {
                                this.closeDetail();
                            } else {
                                panel.addClass('animating').slideDown(options.duration, function() {
                                    panel.removeClass('animating');
                                });
                                project.element.addClass('active');
                            }
                        } else {
                            scope.refreshViewport(project, true);
                            scope.viewport.show(project);
                            project.element.addClass('active');
                        }
                    },
                    closeDetail: function() {
                        this.viewport.el().addClass('animating').slideUp(options.duration, function() {
                            $(this).removeClass('animating')
                        });
                    },
                    prevDetail: function(){ scope.showDetail(scope.model.projectIndex - 1)},
                    nextDetail: function(){ scope.showDetail(scope.model.projectIndex + 1)},
                    dateFormat: function(date) {
                        return date;
                    },
                    refreshViewport: function(project, animate) {
                        if (!project) return;
                        var
                            isVisible = scope.viewport.el().is(':visible'),
                            viewport = scope.viewport.el().hide(),
                            endEl = getEndOfRow(project.element);
                        viewport.show();
                        if (!endEl.is(viewportPos)) {
                            if (animate) {
                                if (isVisible) {
                                    var holder = viewport.clone().insertAfter(viewport).addClass('animating').slideUp(options.duration, function() {
                                        holder.remove();
                                    });
                                }
                                viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                    viewport.removeClass('animating');
                                });
                            }
                            endEl.after(viewport);
                            viewportPos = endEl;
                        } else if (animate && !isVisible) {
                            viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                viewport.removeClass('animating');
                            });
                        }
                    }
                });
                $(window).resize(function() {
                    scope.refreshViewport(scope.viewport.model.project);
                });
            }]).controller('GooglePhotoDetail', ['$scope', '$element', function(scope, element) {
                scope.$parent.viewport = scope;


                var
                    $scoll = $('html,body'),
                    owl = element.find('.images');
                $.extend(scope, {
                    model: {},
                    el: function() {
                        return element;
                    },
                    project: {},
                    close: function() {},
                    show: function(project) {
                        if (this.model.project === project) return;
                        this.model.project = project;
                        owl.find('.owl-stage').children().each(function(index){
                            owl.trigger('remove.owl.carousel', [index]);
                        });

                        $.each(project.images,function(i,img){
                            owl.trigger('add.owl.carousel', [$('<div class="item">').append($('<img>',{src:img})), i]);
                        });
                        owl.trigger('refresh.owl.carousel');
                        setTimeout(function(){
                            $scoll.stop().animate({scrollTop: element.offset().top - 100});
                        },300);
                    }
                });
            }]).directive('googlePhotoElement', function() {
                var propName;
                return function(scope, element, attrs) {
                    propName = propName || $.trim(attrs.ngRepeat.split(' in ')[0]);
                    scope[propName].element = element;
                };
            });
        }, {
            options: {
                module: 'TAM',
                map: function(project) {
                    return project;
                },
                duration: 300
            }
        })
    })(jQuery);
    var exampleData = (function(count) {
        var arr = [];
        for (var i = 0; i < count; i++) {
            arr.push({
                id: i + 1,
                title: 'Tên dịch vụ',
                date: '16 Feb 2017',
                thumbBefore: 'asset/images/Rectangle10.jpg',
                thumbAfter: 'asset/images/after.jpg',
                shortDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                fullDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                images: ['http://via.placeholder.com/1000x1000', 'http://via.placeholder.com/1500x1000', 'http://via.placeholder.com/1200x1000', 'http://via.placeholder.com/1500x1200'],
                colors: [{
                    title: 'Nau',
                    hex: 'ff0000',
                    id: 12453
                }]
            });
        }
        return arr;
    })(12);
    TAM.GooglePhotos({
        module: 'TAM'
    }, exampleData);
</script>