
<?php include 'include/index-top.php';?>
<?php include 'module/module-1.php';?>


	<div id="contentmain">
		<section class="TAM006 bg-flower-right">
			
			<div class="container">
				<div class="section-header">
					<h2 class="section-title">
						về chúng tôi
					</h2>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="item">
							<h3 class="widget-title">Tầm nhìn</h3>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi</p>
							<a href="#" class="btn btn-main">Xem thêm</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<h3 class="widget-title">Tầm nhìn</h3>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi</p>
							<a href="#" class="btn btn-main">Xem thêm</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item">
							<h3 class="widget-title">Tầm nhìn</h3>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi</p>
							<a href="#" class="btn btn-main">Xem thêm</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="TAM007">
			<div class="container">
				<div class="inner">	
					<div class="section-header">
						<h2 class="section-title">
							Branding Points
						</h2>
					</div>
					<p>Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis! Nullam cursus euismod augue. Hymenaeos class dapibus class quam... Proin fames diam: Scelerisque dictum consequat diam pharetra ipsum nec... Eu egestas.</p>
					<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
				</div>
			</div>
		</section>

		<section class="TAM008">
			<div class="section-header">
				<h2 class="section-title">
					Chặng đường phát triển
				</h2>
				<span>Thương hiệu làm đẹp thượng lưu cho tất cả mọi người</span>
			</div>
			<div class="container">
				<div class="counter">
					<div class="container">
						<div class="row">	
							<div class="scroll-overflow">
								<div class="line"><!-- <i class="fa fa-circle" aria-hidden="true"></i> --></div>
								<div class="line"></div>
								<div class="line"></div>
								<div class="line"></div>
							</div>
							<div class="item">
								<span class="count">01</span>
								<div>Năm</div>
								<span class="note">Xây dựng </br>và phát triển</span>
							</div>
							<div class="item">
								<span class="count">03</span>
								<div>Chi nhánh</div>
								<span class="note">Đẳng cấp </br>5 sao</span>
							</div>
							<div class="item">
								<span class="count">1500</span>
								<div>khách hàng</div>
								<span class="note">Thu nhập từ</br>
trên 4 tỷ/năm</span>
							</div>
							<div class="item">
								<span class="count">30</span>
								<div>dịch vụ công nghệ</div>
								<span class="note">Làm đẹp hiện đại,</br>
tiên tiến</span>
							</div>
						</div>
					</div>
				</div><!-- end block__counter -->
			</div>
		</section>
		

		<section class="TAM004 bg-gray">
			<div class="container">
				<div class="row">	
					<div class="block-video col-md-7">
						<a href="#" class="play-video">
		 					<span><i class="fa fa-play" aria-hidden="true"></i></span>
		 				</a>
						<iframe class="video" src="//www.youtube.com/embed/9B7te184ZpQ?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="col-md-5 ">
						<div class="display-table">	
							<div class="block-text table-cell">	
								<div class="section-header">
									<h2 class="section-title">
										Cơ sở vật chất
									</h2>
								</div>
								<p>Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis!
		Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- end TAM004 -->

		<section class="TAM003 bg-left">
			<div class="container">
				<div id="sync2" class="owl-carousel">
				  	<div class="item">Hồ chí minh</div>	  
				  	<div class="item">Cần thơ</div>	  
					<div class="item">Hải phòng</div>
				</div>
				<div id="sync1" class="owl-carousel">
					<div class="block row">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						

					</div>
					
					<div class="block">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>

					</div>
					<div class="block">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>

					</div>
				</div>

				
			</div>
		</section>

	</div>




<?php include 'include/index-bottom.php';?>

