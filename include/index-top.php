<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1">

 
<title>TÂM SPA</title>
<link rel="shortcut icon" href="images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel='stylesheet' href='asset/css/bootstrap.min.css' type='text/css' media='all' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<link rel='stylesheet' href='asset/css/style.css' type='text/css' media='all' />
<script type="text/javascript" src="asset/js/jquery.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>


</head>

<body >


<div id="wrapper">

  <header id="header">
      <div class="max-width clearfix">
          <a href="./" class="logo"><img src="asset/images/logo.png" alt="" /></a>
          <div class="topbar">
            <div class="topbar_top clearfix">
              <div class="topbar_top-left">
                <ul class="menu_top">
                  <li>
                    <a href="./">Trang chủ</a>
                  </li>
                  <li><a href="./02.gioithieu.php">giới thiệu</a></li>
                  <li><a href="./06.news.php">tin tức</a></li>
                  <li><a href="./12.lienhe.php">Liên hệ</a></li>
                </ul>
              </div>
              <div class="topbar_top-right">
                <div class="hotline">
                  <span>1800 2058</span>
                </div>
                <div class="languages">
                  <span>vi</span>
                  <span>en</span>
                </div>
                <span class="menu-btn"><span></span></span> 
              </div>
            </div>
            <div class="topbar_bottom">
                <ul class="menu_main">
                  <li class="parent"><a href="./03.service.php">Nâng cơ</a>
                    <ul>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                      <li><a href="#">Nâng cung chân mày</a></li>
                      <li><a href="#">Nâng cơ đùi</a></li>
                      <li><a href="#">Nâng cơ da cổ</a></li>
                      <li><a href="#">Nâng cơ mông</a></li>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                      <li><a href="#">Nâng cơ vùng mắt</a></li>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                    </ul>
                  </li>
                  <li><a href="./03.service.php">Giảm béo</a></li>
                  <li><a href="./03.service.php">Trị mụn</a></li>
                  <li class="parent"><a href="./03.service.php">Trị nám</a>
                    <ul>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                      <li><a href="#">Nâng cung chân mày</a></li>
                      <li><a href="#">Nâng cơ đùi</a></li>
                      <li><a href="#">Nâng cơ da cổ</a></li>
                      <li><a href="#">Nâng cơ mông</a></li>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                      <li><a href="#">Nâng cơ vùng mắt</a></li>
                      <li><a href="#">Nâng cơ nọng cằm</a></li>
                    </ul>
                  </li>
                  <li><a href="./03.service.php">Chăm sóc da</a></li>
                  <li><a href="./03.service.php">Làm trắng</a></li>
                  <li><a href="./03.service.php">Tàn nhang</a></li>
                  <li><a href="./03.service.php">Dịch vụ khác</a></li>
                </ul>
            </div>
          </div>
      </div>

  </header>


