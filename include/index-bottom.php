

<footer id="footer">
	<div class="container">
		
		<div class="sendform">
			<div class="section-header">	
				<h2 class="title">Đăng ký sử dụng dịch vụ</h2>
				<span>Điền thông tin để biết quà là gì?</span>
			</div>
			<form action="" class="row">
				<div class="item col-md-4">
					<input type="text" placeholder="Họ tên" class="input bg-input">
					<input type="text" id="datepicker" placeholder="Ngày sinh" class="input bg-input">
					<input type="text" placeholder="Điện thoại" class="input bg-input">
				</div>
				<div class="item col-md-4">
					<input type="text" placeholder="Email" class="input bg-input">
					<select name="" id="" class="select bg-input">
						<option>Giới tính</option>
						<option value="Nam">Nam</option>
						<option value="Nữ">Nam</option>
					</select>
					<input type="text" placeholder="Điạ chỉ" class="input bg-input">
				</div>
				<div class="item col-md-4">
					<textarea name="" id="" class="textarea bg-input" rows="3" placeholder="Nội dung"></textarea>
					<button class="btn btn-big">GỬI</button>
				</div>
			</form>
		</div>
		<!-- end sendform-->

		<div class="block-social">
			<ul>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div><!-- end block-social-->

		<div class="footer_content">
			<div class="row">
				<a href="/" class="logo col-md-12"><img src="asset/images/logo.png" alt="" /></a>
				<div class="col-md-4 block-contact">
					<ul class="">	
						<li>0977 954 800</li>
						<li>LoremIpsum@gmail.com</li>
						<li>www.Lorem Ipsum</li>
						<li>www.Lorem Ipsum</li>
					</ul>
					<a href="#" class="btn btn-big">Liên hệ với chúng tôi</a>
				</div>
				<div class="col-md-8 footer_menu">
					<div class="footer_menu-top clearfix">
						<div class="item">
							<ul>
								<li><a href="#">Trang chủ</a></li>
								<li><a href="#">Giới thiệu</a></li>
								<li><a href="#">Tin tức</a></li>
							</ul>
						</div>
						<div class="item">
							<ul>
								<li><a href="#">Dịch vụ </a></li>
								<li><a href="#">Nâng cơ</a></li>
								<li><a href="#">Giảm béo</a></li>
								<li><a href="#">Trị mụn</a></li>
							</ul>
						</div>
						<div class="item">
							<ul>
								<li><a href="#">Trị nám</a></li>
								<li><a href="#">Chăm sóc da</a></li>
								<li><a href="#">Làm trắng</a></li>
								<li><a href="#">Tạo hình</a></li>
							</ul>
						</div>
						<div class="item">
							<ul>
								<li><a href="#">Bảng giá</a></li>
								<li><a href="#">Thành viên</a></li>
								<li><a href="#">Liên hệ</a></li>
							</ul>
						</div>
					</div>
					<p>
						Kết quả tuỳ thuộc vào cơ địa của mỗi người.
					</p>
					<span>Điều khoản và chính sách bảo mật</span>
					<a href="#" class="btn btn-big">Liên hệ với chúng tôi</a>
				</div>

			</div>
		</div>
	</div>
	<div class="footer_bottom">
		design by Spa & Clinic
	</div>
</footer>




<!-- <div class="fb-messenger">
	<span class="btnFb"><i class="fa fa-comment" aria-hidden="true"></i>  Gửi tin nhắn cho chúng tôi </span>
	<div  class='fb-page' data-adapt-container-width='true' data-height='300' data-hide-cover='false' data-href='https://www.facebook.com/Toavn/' data-show-facepile='true' data-show-posts='false' data-small-header='false' data-tabs='messages' data-width='850'></div>
</div> -->
				


</div>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type='text/javascript' src='asset/js/owl.carousel.min.js'></script> 
<script type='text/javascript' src='asset/js/bootstrap.min.js'></script>
<script type='text/javascript' src="https://cdn.jsdelivr.net/npm/owl.carousel2.thumbs@0.1.8/dist/owl.carousel2.thumbs.min.js"></script>
<script type='text/javascript' src='asset/js/script.js'></script>
<script type='text/javascript' src='asset/js/user.js'></script>
<script>
	$( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>

</body></html>