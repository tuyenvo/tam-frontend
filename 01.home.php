<?php include 'module/module-0.php';?>
<?php include 'include/index-top.php';?>
<?php include 'module/module-1.php';?>
<?php include 'module/module-2.php';?>

	<div id="contentmain">
		<section id="google_photo" ng-app="TAM" ng-controller="GooglePhotos" class="home-service">
	        <div class="container">
	        	<div class="section-header">
					<h2 class="section-title">
						dịch vụ nổi bật
					</h2>
				</div>
		        <div class="photos row block-1 ">
		            <div class="col-sm-6 col-xs-12 col-md-3 photo" ng-repeat="project in model.projects" ng-click="showDetail($index)" google-photo-element>
		                <div class="item">
		                    <div class="item_img">
		                        <img ng-src="{{project.thumbBefore}}" />
		                        <img ng-src="{{project.thumbAfter}}" class="img-after" />
		                    </div>
	                        <div class="item_title">
	                            {{project.title}}
	                        </div>
	                        <a  class="overflow"><i class="fa fa-plus" aria-hidden="true"></i></a>
		                </div>
		            </div>
		        </div>
		        
		        <div id="google_photo_detail" ng-controller="GooglePhotoDetail" class="content row " style="display:none;">
		           	                    
	                <div class="text">
	                	Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis! Nullam cursus euismod augue. Hymenaeos class dapibus class quam... Proin fames diam: Scelerisque dictum consequat diam pharetra ipsum nec... Eu egestas.
	                </div>
	                <div class="owl-carousel images s-dots">   
	                    <div class="item">
	                    	<div class="thumb">
	                    		<img data-src="{{src}}" />
	                    	</div>
	                    	<span>{{description}}</span>
	                    	<a href="{{link}}" class="btn btn-main">Xem thêm</a>
	                    </div>
	               	</div>                     
		            
		        </div>
				
	        </div>
	    </section>
		
		<section class="TAM005 bg-flower-left">
			<div class="container">
				<div class="row">	
					
					<div class="col-md-5">
						<div class="slide-one owl-carousel s-dots">	
							<div class="item">
								<div class="thumb">
									<img src="asset/images/khachhang.png" alt="">
								</div>
								<div class="info">
									<h3 class="name">Tên khách hàng</h3>
									<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur</p>
								</div>
							</div>
							<div class="item">
								<div class="thumb">
									<img src="asset/images/khachhang.png" alt="">
								</div>
								<div class="info">
									<h3 class="name">Tên khách hàng</h3>
									<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur</p>
								</div>
							</div>
							<div class="item">
								<div class="thumb">
									<img src="asset/images/khachhang.png" alt="">
								</div>
								<div class="info">
									<h3 class="name">Tên khách hàng</h3>
									<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur</p>
								</div>
							</div>
						</div>
					</div>
					<div class="block-video iframe block-video-right col-md-7">
						<a href="#" class="play-video">
		 					<span><i class="fa fa-play" aria-hidden="true"></i></span>
		 				</a>
						<iframe class="video" src="//www.youtube.com/embed/9B7te184ZpQ?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section><!-- end home-service -->

		<section class="TAM004 bg-gray">
			<div class="container">
				<div class="row">	
					<div class="block-video iframe col-md-7">
						<a href="#" class="play-video">
		 					<span><i class="fa fa-play" aria-hidden="true"></i></span>
		 				</a>
						<iframe class="video" src="//www.youtube.com/embed/9B7te184ZpQ?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="col-md-5 ">
						<div class="display-table">	
							<div class="block-text table-cell">	
								<div class="section-header">
									<h2 class="section-title">
										Cơ sở vật chất
									</h2>
								</div>
								<p>Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis!
		Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- end TAM004 -->

		<section class="TAM003 bg-left">
			<div class="container">
				<div id="sync2" class="owl-carousel">
				  	<div class="item">Hồ chí minh</div>	  
				  	<div class="item">Cần thơ</div>	  
					<div class="item">Hải phòng</div>
				</div>
				<div id="sync1" class="owl-carousel">
					<div class="block row">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						

					</div>
					
					<div class="block">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>

					</div>
					<div class="block">
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="block-item " style="background-image: url(asset/images/Rectangle23.jpg);">	
								<div class="overflow">
									<h3 class="title"><a href="#">spa vip</a></h3>
									<p class="overflow-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
								</div>
								<div class="name">
									PHÒNG XÔNG HƠI
								</div>
							</div>
						</div>

					</div>
				</div>

				
			</div>
		</section>

		<section class="TAM002 ">
			<div class="container">
				<div class="slide-one owl-carousel s-dots s-nav">	
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
					<div class="item clearfix">
						<div class="thumb col-md-5">
							<img src="asset/images/khachhang.png" alt="">
						</div>
						<div class="info col-md-7">
							<h3 class="name">Tên khách hàng</h3>
							<span>Dịch vụ đã sử dụng</span>
							<p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales...</p>
						</div>
					</div><!-- end item -->
				</div>
			</div>
		</section><!-- end TAM002 -->
		<section id="home_blog" class="home-blog">
            <div class="section-header">
                <h2 class="section-title">
                    Tin tức
                </h2>
            </div>
            <div class="row grid-space-0">

                <div class="col-md-3 item">
                    <div class="thumb">
                        <img src="asset/images/Rectangle 45.jpg" alt="">
                        <div class="overflow">
                            <div class="meta">
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i> 25 Oct 2017</span>
                                <span><i class="fa fa-eye" aria-hidden="true"></i> 12</span>
                            </div>
                            <h2 class="title">Adwords Keyword Research For Beginners</h2>
                        </div>
                    </div>
                    <div class="info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                        <a href="#" class="btn btn-main">Xem thêm</a>
                    </div>
                </div>
                <!-- end item -->

                <div class="col-md-3 item">
                    <div class="thumb">
                        <img src="asset/images/Rectangle 45.jpg" alt="">
                        <div class="overflow">
                            <div class="meta">
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i> 25 Oct 2017</span>
                                <span><i class="fa fa-eye" aria-hidden="true"></i> 12</span>
                            </div>
                            <h2 class="title">Adwords Keyword Research For Beginners</h2>
                        </div>
                    </div>
                    <div class="info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                        <a href="#" class="btn btn-main">Xem thêm</a>
                    </div>
                </div>
                <!-- end item -->

                <div class="col-md-3 item">
                    <div class="thumb">
                        <img src="asset/images/Rectangle 45.jpg" alt="">
                        <div class="overflow">
                            <div class="meta">
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i> 25 Oct 2017</span>
                                <span><i class="fa fa-eye" aria-hidden="true"></i> 12</span>
                            </div>
                            <h2 class="title">Adwords Keyword Research For Beginners</h2>
                        </div>
                    </div>
                    <div class="info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                        <a href="#" class="btn btn-main">Xem thêm</a>
                    </div>
                </div>
                <!-- end item -->

                <div class="col-md-3 item">
                    <div class="thumb">
                        <img src="asset/images/Rectangle 45.jpg" alt="">
                        <div class="overflow">
                            <div class="meta">
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i> 25 Oct 2017</span>
                                <span><i class="fa fa-eye" aria-hidden="true"></i> 12</span>
                            </div>
                            <h2 class="title">Adwords Keyword Research For Beginners</h2>
                        </div>
                    </div>
                    <div class="info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                        <a href="#" class="btn btn-main">Xem thêm</a>
                    </div>
                </div>
                <!-- end item -->
            </div>
		</section><!-- end home-blog -->

		<section class="TAM001">
			<div class="container">
				<div class="section-header">
					<h2 class="section-title">
						Các chương trình khuyến mãi
					</h2>
					<span>(Dành cho thành viên)</span>
				</div>
				<div class="slide-one owl-carousel s-dots">
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/Rectangle 44.jpg" alt="">
					</div>

				</div>
			</div>
		</section><!-- end TAM001 -->

		<section class="home-partner">
			<div class="container-fluid">
				<div class="section-header">
					<h2 class="section-title">
						Đối tác của chúng tôi
					</h2>
					
				</div>
				<div class="owl-carousel s-nav">
					<div class="item">
						<img src="asset/images/1.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/2.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/1.jpg" alt="">
					</div>

					<div class="item">
						<img src="asset/images/2.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/1.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/2.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/1.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/2.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/1.jpg" alt="">
					</div>
					<div class="item">
						<img src="asset/images/2.jpg" alt="">
					</div>

				</div>
			</div>
		</section><!-- end home-partner -->

		<section class="home-connect bg-gray">
			<div class="section-header">
				<h2 class="section-title">
					Kết nối với chúng tôi
				</h2>
				<span>Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis! Nullam cursus euismod augue. Hymenaeos class dapibus class quam... Proin fames diam: Scelerisque dictum consequat diam pharetra ipsum nec... Eu egestas.</span>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3 item" style="background-image: url('asset/images/cn1.jpg');">
						
					</div>
					<div class="col-md-3 item" style="background-image: url('asset/images/cn2.jpg');">
						
					</div>
					<div class="col-md-3 item" style="background-image: url('asset/images/cn3.jpg');">
						
					</div>
					<div class="col-md-3 item" style="background-image: url('asset/images/cn4.jpg');">
						
					</div>
				</div>
				<div class="text-center">
					<a href="#" class="btn btn-main">Xem thêm</a>
				</div>
				
			</div>
		</section><!-- end home-connect -->

		

	</div>

<script>
	/* parallax */
    function parallax() {
        var scrollPos = $(window).scrollTop();
        $('.welcome').css('backgroundPosition', "50% " + Math.round(($('.welcome').offset().top - scrollPos) * 0.5) + "px");
    }

    $(window).bind('scroll', function() {
        parallax();
    });
</script>


<?php include 'include/index-bottom.php';?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript">window.TAM = window.TAM || {};
    TAM.GooglePhotos = (function($) {
        var mapData = function(data, map) {
            $.each(map, function(key, val) {
                if (data[key] != undefined) {
                    data[val] = data[key];
                    delete data[key];
                }
            });
            return data;
        }
        return $.extend(function(options, data) {
            var self = arguments.callee;
            options = $.extend({}, self.options, options);
            var app = angular.module(options.module, []);
            var getEndOfRow = function(element) {
                var endEl = $(element),
                    currentTop = endEl.position().top,
                    nextEl;
                while (true) {
                    nextEl = endEl.next();
                    while (nextEl.length && !nextEl.hasClass('photo')) nextEl = nextEl.next();
                    if (!nextEl.length) break;
                    if (nextEl.position().top > currentTop) break;
                    endEl = endEl.next();
                }
                return endEl;
            }
            app.controller('GooglePhotos', ['$scope', function(scope) {
                var viewPort, viewportPos, transitionEvts = 'transitionend webkitTransitionEnd oTransitionEnd';
                $.extend(scope, {
                    model: {
                        projects: (data || []).map(function(project) {
                            return mapData(project, options.map);
                        })
                    },
                    viewport: {},
                    showDetail: function(index) {
                        index = index < 0?0:index;
                        index = index > scope.model.projects.length - 1? scope.model.projects.length - 1: index;
                        scope.model.projectIndex = index;
                        var panel = scope.viewport.el(),project = scope.model.projects[index];
                        project.element.parent().children('.active').removeClass('active');
                        if (project === scope.viewport.model.project) {
                            if (panel.is(':visible')) {
                                this.closeDetail();
                            } else {
                                panel.addClass('animating').slideDown(options.duration, function() {
                                    panel.removeClass('animating');
                                });
                                project.element.addClass('active');
                            }
                        } else {
                            scope.refreshViewport(project, true);
                            scope.viewport.show(project);
                            project.element.addClass('active');
                        }
                    },
                    closeDetail: function() {
                        this.viewport.el().addClass('animating').slideUp(options.duration, function() {
                            $(this).removeClass('animating')
                        });
                    },
                    prevDetail: function(){ scope.showDetail(scope.model.projectIndex - 1)},
                    nextDetail: function(){ scope.showDetail(scope.model.projectIndex + 1)},
                    dateFormat: function(date) {
                        return date;
                    },
                    refreshViewport: function(project, animate) {
                        if (!project) return;
                        var
                            isVisible = scope.viewport.el().is(':visible'),
                            viewport = scope.viewport.el().hide(),
                            endEl = getEndOfRow(project.element);
                        viewport.show();
                        if (!endEl.is(viewportPos)) {
                            if (animate) {
                                if (isVisible) {
                                    var holder = viewport.clone().insertAfter(viewport).addClass('animating').slideUp(options.duration, function() {
                                        holder.remove();
                                    });
                                }
                                viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                    viewport.removeClass('animating');
                                });
                            }
                            endEl.after(viewport);
                            viewportPos = endEl;
                        } else if (animate && !isVisible) {
                            viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                viewport.removeClass('animating');
                            });
                        }
                    }
                });
                $(window).resize(function() {
                    scope.refreshViewport(scope.viewport.model.project);
                });
            }]).controller('GooglePhotoDetail', ['$scope', '$element', function(scope, element) {
                scope.$parent.viewport = scope;


                var
                    $scoll = $('html,body'),
                    owl = element.find('.images');
                    itemTemplate = owl.children().get(0).outerHTML;

				
                $.extend(scope, {
                    model: {},
                    el: function() {
                        return element;
                    },
                    project: {},
                    close: function() {},
                    show: function(project) {
                        if (this.model.project === project) return;
                        this.model.project = project;
                        owl.find('.owl-stage').children().each(function(index){
                            owl.trigger('remove.owl.carousel', [index]);
                        });

                        $.each(project.services,function(i,img){
                        	var item = $(itemTemplate.replace(/{{(.+?)}}/g,function(key, prop){
                        		return img[prop];
                        	}));
                        	item.find('img').each(function(){this.src = $(this).data('src')});
                            owl.trigger('add.owl.carousel', [item, i]);
                        });
                        owl.trigger('refresh.owl.carousel');
                        setTimeout(function(){
                            $scoll.stop().animate({scrollTop: element.offset().top - 100});
                        },300);
                    }
                });
            }]).directive('googlePhotoElement', function() {
                var propName;
                return function(scope, element, attrs) {
                    propName = propName || $.trim(attrs.ngRepeat.split(' in ')[0]);
                    scope[propName].element = element;
                };
            });
        }, {
            options: {
                module: 'TAM',
                map: function(project) {
                    return project;
                },
                duration: 300
            }
        })
    })(jQuery);
    var exampleData = (function(count) {
        var arr = [];
        for (var i = 0; i < count; i++) {
            arr.push({
                id: i + 1,
                title: 'Tên dịch vụ',
                thumbBefore: 'asset/images/Rectangle10.jpg',
                thumbAfter: 'asset/images/after.jpg',
                description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                services: [
                	{
                		src: 'asset/images/nguoi-mau.png',
                		description: "Description",
                		link: 'http://google.com.vn'
                	},
                	{
                		src: 'asset/images/nguoi-mau.png',
                		description: "Description",
                		link: 'http://google.com.vn'
                	},
                	{
                		src: 'asset/images/nguoi-mau.png',
                		description: "Description",
                		link: 'http://google.com.vn'
                	},
                	{
                		src: 'asset/images/nguoi-mau.png',
                		description: "Description",
                		link: 'http://google.com.vn'
                	},
                	{
                		src: 'asset/images/nguoi-mau.png',
                		description: "Description",
                		link: 'http://google.com.vn'
                	}
                ]
            });
        }
        return arr;
    })(12);
    TAM.GooglePhotos({
        module: 'TAM'
    }, exampleData);
</script>