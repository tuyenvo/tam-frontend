<?php include 'include/index-top.php';?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
	<div id="contentmain" class="news-detail">
        <div class="breadcrumb">
            <div class="container">
                <ul class="block-breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li><a href="#">Tin tức</a></li>
                    <li><a href="#">Bài viết chi tiết</a></li>
                </ul>
            </div>
        </div>

        <section id="detail_content" class="detail">
            <div class="space-breadcrumb"></div>
            <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="detail_content">
                                <h3 class="detail_content_title">
                                    Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis!
                                </h3>
                                <p class="detail_content_summary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dignissim velit taciti litora a hendrerit eu; Class metus velit urna per sit nibh.
                                    Elementum libero proin tellus; Porta elementum enim sit venenatis per parturient sit. Tempus augue mus pretium fusce tristique ac?
                                </p>
                                <div class="detail_content_desc">
                                    <p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales... Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis! Nullam cursus euismod augue. Hymenaeos class dapibus class quam... Proin fames diam: Scelerisque dictum consequat diam pharetra ipsum nec... Eu egestas.</p>
                                    <p class="text-center"><img src="asset/images/news-img.jpg" alt="news-img"></p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Metus dignissim velit taciti litora a hendrerit eu; Class metus velit urna per sit nibh. Elementum libero proin tellus; Porta elementum enim sit venenatis per parturient sit. Tempus augue mus pretium fusce tristique ac?</p>
                                    <p>Montes hymenaeos commodo vitae auctor odio pretium hac. Nonummy sociis metus cursus habitant facilisi, et cum etiam nonummy... Fermentum nascetur pulvinar nascetur... Sociosqu facilisi eu pretium! Erat non eget primis primis habitasse ultrices primis... Porttitor sodales... Ornare pellentesque tempor tempor, sollicitudin sociis duis dapibus... Magna consequat a duis lectus neque: Interdum aliquam sagittis! Nullam cursus euismod augue. Hymenaeos class dapibus class quam... Proin fames diam: Scelerisque dictum consequat diam pharetra ipsum nec... Eu egestas.</p>
                                </div>
                                <div class="detail_content_count">
                                    <i class="material-icons">remove_red_eye</i><span class="view">51</span>
                                    <i class="material-icons">mode_comment</i><span class="comment">12</span>
                                </div>
                                <div class="detail_content_comment">
                                    <h4>Góc bình luận</h4>
                                    <div class="fb-comments" data-href="http://tt.mangoads.vn/tam-frontend/07.news-detail.php" width="100%" data-numposts="5"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail_sidebar">
                                <div class="detail_sidebar_title">
                                    <h6 class="mb-0">Dịch vụ liên quan</h6>
                                </div>
                                <div class="detail_sidebar_content sidebar_service">
                                    <div class="sidebar_service_block">
                                        <div class="sidebar_service_img">
                                            <a href="#">
                                                <img src="asset/images/detail-service.png" alt="">
                                                <img ng-src="asset/images/after.jpg" class="img-after" src="asset/images/after.jpg">
                                            </a>
                                        </div>
                                        <a href="#" class="sidebar_service_title">DỊCH VỤ 1</a>
                                    </div>
                                    <div class="sidebar_service_block">
                                        <div class="sidebar_service_img">
                                            <a href="#">
                                                <img src="asset/images/detail-service.png" alt="">
                                                <img ng-src="asset/images/after.jpg" class="img-after" src="asset/images/after.jpg">
                                            </a>
                                        </div>
                                        <a href="#" class="sidebar_service_title">DỊCH VỤ 1</a>
                                    </div>
                                    <div class="sidebar_service_block">
                                        <div class="sidebar_service_img">
                                            <a href="#">
                                                <img src="asset/images/detail-service.png" alt="">
                                                <img ng-src="asset/images/after.jpg" class="img-after" src="asset/images/after.jpg">
                                            </a>
                                        </div>
                                        <a href="#" class="sidebar_service_title">DỊCH VỤ 1</a>
                                    </div>
                                </div>
                                <div class="detail_sidebar_title">
                                    <h6 class="mb-0">VIDEO NHIỀU NGƯỜI XEM NHẤT</h6>
                                </div>
                                <div class="detail_sidebar_content">
                                    <div class="block-video video-modal">
                                        <a data-toggle="modal" data-target="#videoModal" class="play-video">
                                            <span><i class="material-icons">play_arrow</i></span>
                                        </a>
                                        <img src="asset/images/news-1.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
        <div id="videoModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="video_title">Video được xem nhiều nhất</h5>
                    </div>
                    <div class="modal-body">
                        <iframe class="video" height="100%" width="100%" src="//www.youtube.com/embed/9B7te184ZpQ?rel=0&enablejsapi=1&version=3&playerapiid=ytplayer" allowscriptaccess="always" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </div>

		<section id="detail_related" class="home-blog news_list">
			<div class="container">
                <div class="section-header">
                    <h2 class="section-title">
                        Tin tức liên quan
                    </h2>
                </div>
				<div class="row">
					<div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-2.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
					</div>
					<!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-3.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="asset/images/news-4.jpg" alt="">
                                <div class="overflow">
                                    <div class="meta">
                                        <span><i class="material-icons">event</i> 25 Oct 2017</span>
                                        <span><i class="material-icons">remove_red_eye</i> 12</span>
                                    </div>
                                    <h2 class="title">Adwords Keyword Research For Beginners</h2>
                                </div>
                            </div>
                            <div class="info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore…</p>
                                <a href="#" class="btn btn-main">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <!-- end item -->
				</div>
			</div>
		</section><!-- end home-blog -->
	</div>




<?php include 'include/index-bottom.php';?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript">window.TAM = window.TAM || {};
    TAM.GooglePhotos = (function($) {
        var mapData = function(data, map) {
            $.each(map, function(key, val) {
                if (data[key] != undefined) {
                    data[val] = data[key];
                    delete data[key];
                }
            });
            return data;
        }
        return $.extend(function(options, data) {
            var self = arguments.callee;
            options = $.extend({}, self.options, options);
            var app = angular.module(options.module, []);
            var getEndOfRow = function(element) {
                var endEl = $(element),
                    currentTop = endEl.position().top,
                    nextEl;
                while (true) {
                    nextEl = endEl.next();
                    while (nextEl.length && !nextEl.hasClass('photo')) nextEl = nextEl.next();
                    if (!nextEl.length) break;
                    if (nextEl.position().top > currentTop) break;
                    endEl = endEl.next();
                }
                return endEl;
            }
            app.controller('GooglePhotos', ['$scope', function(scope) {
                var viewPort, viewportPos, transitionEvts = 'transitionend webkitTransitionEnd oTransitionEnd';
                $.extend(scope, {
                    model: {
                        projects: (data || []).map(function(project) {
                            return mapData(project, options.map);
                        })
                    },
                    viewport: {},
                    showDetail: function(index) {
                        index = index < 0?0:index;
                        index = index > scope.model.projects.length - 1? scope.model.projects.length - 1: index;
                        scope.model.projectIndex = index;
                        var panel = scope.viewport.el(),project = scope.model.projects[index];
                        project.element.parent().children('.active').removeClass('active');
                        if (project === scope.viewport.model.project) {
                            if (panel.is(':visible')) {
                                this.closeDetail();
                            } else {
                                panel.addClass('animating').slideDown(options.duration, function() {
                                    panel.removeClass('animating');
                                });
                                project.element.addClass('active');
                            }
                        } else {
                            scope.refreshViewport(project, true);
                            scope.viewport.show(project);
                            project.element.addClass('active');
                        }
                    },
                    closeDetail: function() {
                        this.viewport.el().addClass('animating').slideUp(options.duration, function() {
                            $(this).removeClass('animating')
                        });
                    },
                    prevDetail: function(){ scope.showDetail(scope.model.projectIndex - 1)},
                    nextDetail: function(){ scope.showDetail(scope.model.projectIndex + 1)},
                    dateFormat: function(date) {
                        return date;
                    },
                    refreshViewport: function(project, animate) {
                        if (!project) return;
                        var
                            isVisible = scope.viewport.el().is(':visible'),
                            viewport = scope.viewport.el().hide(),
                            endEl = getEndOfRow(project.element);
                        viewport.show();
                        if (!endEl.is(viewportPos)) {
                            if (animate) {
                                if (isVisible) {
                                    var holder = viewport.clone().insertAfter(viewport).addClass('animating').slideUp(options.duration, function() {
                                        holder.remove();
                                    });
                                }
                                viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                    viewport.removeClass('animating');
                                });
                            }
                            endEl.after(viewport);
                            viewportPos = endEl;
                        } else if (animate && !isVisible) {
                            viewport.hide().addClass('animating').slideDown(options.duration, function() {
                                viewport.removeClass('animating');
                            });
                        }
                    }
                });
                $(window).resize(function() {
                    scope.refreshViewport(scope.viewport.model.project);
                });
            }]).controller('GooglePhotoDetail', ['$scope', '$element', function(scope, element) {
                scope.$parent.viewport = scope;


                var
                    $scoll = $('html,body'),
                    owl = element.find('.images');
                $.extend(scope, {
                    model: {},
                    el: function() {
                        return element;
                    },
                    project: {},
                    close: function() {},
                    show: function(project) {
                        if (this.model.project === project) return;
                        this.model.project = project;
                        owl.find('.owl-stage').children().each(function(index){
                            owl.trigger('remove.owl.carousel', [index]);
                        });

                        $.each(project.images,function(i,img){
                            owl.trigger('add.owl.carousel', [$('<div class="item">').append($('<img>',{src:img})), i]);
                        });
                        owl.trigger('refresh.owl.carousel');
                        setTimeout(function(){
                            $scoll.stop().animate({scrollTop: element.offset().top - 100});
                        },300);
                    }
                });
            }]).directive('googlePhotoElement', function() {
                var propName;
                return function(scope, element, attrs) {
                    propName = propName || $.trim(attrs.ngRepeat.split(' in ')[0]);
                    scope[propName].element = element;
                };
            });
        }, {
            options: {
                module: 'TAM',
                map: function(project) {
                    return project;
                },
                duration: 300
            }
        })
    })(jQuery);
    var exampleData = (function(count) {
        var arr = [];
        for (var i = 0; i < count; i++) {
            arr.push({
                id: i + 1,
                title: 'Tên dịch vụ',
                date: '16 Feb 2017',
                thumbBefore: 'asset/images/Rectangle10.jpg',
                thumbAfter: 'asset/images/after.jpg',
                shortDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                fullDesc: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                images: ['http://via.placeholder.com/1000x1000', 'http://via.placeholder.com/1500x1000', 'http://via.placeholder.com/1200x1000', 'http://via.placeholder.com/1500x1200'],
                colors: [{
                    title: 'Nau',
                    hex: 'ff0000',
                    id: 12453
                }]
            });
        }
        return arr;
    })(12);
    TAM.GooglePhotos({
        module: 'TAM'
    }, exampleData);
</script>